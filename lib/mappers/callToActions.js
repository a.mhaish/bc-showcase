import CallToAction from '../containers/Portfolio/CallToAction';

const resolveComponents = () => {
  return {
    403384: {
      render: props => <CallToAction {...props} />,
      name: 'Portfolio Call to Action',
      category: 'Call to action',
    },
  };
};
export default resolveComponents;
