import React, { Fragment } from 'react';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import { saasTheme } from '../../theme/saas';
import { ResetCSS } from '../../assets/css/style';
import { GlobalStyle, ContentWrapper } from '../containers/Saas/saas.style';

import BCComponent from '../pool';
import BCLink from '../containers/_Shared/BCLink';
import QUERY from '../graphql/showcase-query';
import { useQuery } from '@apollo/react-hooks';

export default props => {
  const { config, path } = props;
  try {
    const queryResult = useQuery(QUERY, {
      variables: {
        rootId: config.root,
        deep: config.deep,
        path,
      },
    });
    if (queryResult.loading) {
      return <div>Loading ... </div>;
    }
    if (queryResult.error) {
      return <div>Error loading data: {queryResult.error.message}</div>;
    }
    if (
      queryResult.data &&
      queryResult.data.contents &&
      queryResult.data.contents.length > 0
    ) {
      let data = queryResult.data.contents[0];
      let rootModelData = data.data.reduce(function(map, obj) {
        map[obj.Key] = obj.Value;
        return map;
      }, {});
      
      console.log(data);
      let isAR = config.lang == 'AR';
      return (
        <ThemeProvider theme={saasTheme(rootModelData)}>
          <Fragment>
            <Head>
              <title>{data.title}</title>
              <meta name="Description" content={data.metaDescription} />
              <meta name="theme-color" content="#ec5555" />
              {/* Load google fonts */}
              <link
                href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
                rel="stylesheet"
              />
            </Head>
            <ResetCSS />
            <GlobalStyle />
            <ContentWrapper>
              {data.children &&
                data.children
                  .filter(m => m.online)
                  .map((model, index) => (
                    <BCComponent
                      key={`BCComponent${index}`}
                      modelId={model.modelId}
                      model={model}
                      isAR={isAR}
                    />
                  ))}
              <BCLink />
            </ContentWrapper>
          </Fragment>
        </ThemeProvider>
      );
    } else {
      return <div>No data found</div>;
    }
  } catch (ex) {
    return <div>Error: {ex.message}</div>;
  }
};
